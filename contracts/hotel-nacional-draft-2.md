# CONTRATO DE PRESTAÇÃO DE SERVIÇO E DE COMERCIALIZAÇÃO DE DIÁRIAS HOTELEIRAS

Pelo presente instrumento particular de prestação de serviços, de um
lado como **CONTRATADO**, como doravante será denominado, **HOTEL
NACIONAL INN CURITIBA LTDA**, pessoa jurídica de direito privado,
inscrita no CNPJ sob o no 14.661.448/0001-60, estabelecida à Rua
Lourenço Pinto, nº 458. Centro. Curitiba/PR. CEP 80010-160. Telefone
(41) 3322-4242, neste ato representado por seu Gerente Geral, Sr.
Francisco Pedro Azambuja Vieira; e de outro lado como **CONTRATANTE**,
doravante será denominado, **INSTITUTO PARA CONSERVAÇÃO DE TECNOLOGIAS
LIVRES**, pessoa jurídica de direto privado inscrita no CNPJ sob o nº,
estabelecida à CEP . Telefone (41), sendo neste ato representada pelo
Sr. Antonio Soares de Azevedo Terceiro, inscrito no CPF sob o nº XXXX e
portador do RG nº XXXX, fazem entre si, certo e ajustado, o presente
Contrato Particular, mediante as cláusulas e condições abaixo
estipuladas, de amplo conhecimento das partes, que aceitam e se obrigam
por si e seus sucessores a qualquer título.

## I -- OBJETO

**CLÁUSULA PRIMEIRA --**  O presente contrato tem por objetivo, pelo
CONTRATADO, a disponibilização de apartamentos e salas para o evento da
CONTRATANTE que será realizado no período de 14 a 29 de julho de 2019,
juntamente com eventual fornecimento de serviços de alimentos e bebidas
se solicitado pela CONTRATANTE, conforme bloqueios a seguir.

*Parágrafo Primeiro --* 43 quartos reservados de 14 a 29 de julho conforme abaixo:

* 11 apartamentos luxo triplos: com 03 camas solteiro cada.
* 22 apartamentos luxo família: com 02 camas casal e 01 cama solteiro cada.
* 10 apartamentos luxo duplo: com 02 camas casal cada.

*Parágrafo Segundo --* Adição de mais 27 quartos reservados de 20 a 29 de julho,
totalizando 70 quartos, conforme abaixo:

* 02 apartamentos luxo quádruplos: com 04 camas solteiro cada.
* 11 apartamentos standard triplos: com 03 camas de solteiro cada.
* 12 apartamentos standard duplo: com 01 cama casal e 01 cama solteiro cada.
* 02 apartamentos luxo adaptados: com 02 camas casal cada.

*Parágrafo Terceiro --* 04 salas reservadas de 14 a 29 de julho conforme abaixo:

* Sala Paraná no 13º andar.
* Salas Morretes e Antonia no 12º andar.
* Sala Lapa no térreo.

*** Pedir para usar o Paraná + Morretes/Antonina como cordesia na debcamp pq teriamos que fechar 40 quartos***

## II -- CONDIÇÕES GERAIS

**CLÁUSULA SEGUNDA --** Dos preços e informações gerais.

*Parágrafo Primeiro --* As partes acima acordam os seguintes valores
para a realização das hospedagens e evento:

Apartamentos:

1. Apartamento single: R$ 153,00 (cento e cinquenta e três reais) por pessoa por dia.
1. Apartamentos duplos, triplos e quádruplos: R$ 85,00 (oitenta e cinco reais) por pessoa por dia.

Salas

1. Salão Paraná: R$ 900,00 (novecentos reais) a diária.
1. Salas Curitiba, Londrina e Maringá: R$ 380,00 (trezentos e oitenta reais) a diária.
1. Salas Guaíra e Iguaçu: R$ 280,00 (duzentos e oitenta reais) a diária.
1. Salas Morretes e Antonia R$ ?? a diária
1. Sala Lapa R$ ?? a diária

Outros:

1. Galão de Água Mineral 20 litros: R$ 28,50 (vinte e oito reais e cinquenta centavos) a unidade.
1. Garrafa de Café: R$ 17,00 (dezessete reais) a unidade.

*Parágrafo Segundo --* No valor das diárias dos apartamentos estão incluídos:

-   Café da manhã.
-   Acesso à internet wifi.
-   Acesso as dependências de lazer do hotel (sauna, academia, piscina).

Nas diárias das salas de eventos somente o espaço com mesas e cadeiras,
demais equipamentos audiovisuais deverão ser locados.

*Parágrafo Terceiro --* As partes acordam ainda que outros serviços aqui
não descritos e solicitados pela CONTRATANTE seguirão a tabela de preços
do hotel vigente à época da hospedagem e o orçamento enviado, que fica
fazendo parte integrante deste como Anexo Contratual I.

*Parágrafo Quarto --* As salas de eventos terão suas diárias como
cortesia na proporção das hospedagens abaixo, totalizando no máximo 04
(quatro) diárias de cortesia por dia.

1. Salão Paraná -- a cada 25 (vinte e cinco) apartamentos hospedados por dia.
1. Demais salas -- a cada mais 15 (quinze) apartamentos hospedados por dia.

*Parágrafo Quinto --* O CONTRATANTE está autorizado a vender sua própria
cerveja após as 18:00 no 13º andar mediante uma taxa de limpeza diária de R$
120,00 (cento e vinte reais), a qual será lançada na conta master do evento.

*Parágrafo Sexto --* Também está autorizada a instalação de link de
internet no 12º e 13º andares, devendo os custos correrem exclusivamente
por conta do CONTRATANTE.

*Parágrafo Sétimo --* O CONTRATADO irá disponibilizar à CONTRATANTE um
promo-code a ser utilizado para reserva de apartamentos somente no
período do evento para que a mesma repasse à outras pessoas que quiserem
participar do evento e não estiverem no rooming list oficial.

*Parágrafo Oitavo --* O promo-code será válido somente para reservas
efetuadas no site oficial da Rede -- www.nacionalinn.com.br -- e para
todos os hoteis da rede em Curitiba - PR.

## III -- OBRIGAÇÕES

**CLÁUSULA TERCEIRA --** Do CONTRATADO.

*Parágrafo Primeiro --* O CONTRATADO obriga-se a disponibilizar o número de
apartamentos contratados para hospedar os passageiros da CONTRATANTE,
bem como disponibilizar as salas de eventos e demais serviços de alimentos e
bebidas quando contratados.

*Parágrafo Segundo --* O CONTRATADO obriga-se a colocar em todos os alimentos
servidor durante o café da manhã placas indicativas em inglês com os respectivos
nomes, e no caso de alimentos preparados incluir a descrição dos ingrediantes
também em inglês.

**CLÁUSULA QUARTA --** Do CONTRATANTE.

*Parágrafo Primeiro --* A CONTRATANTE obriga-se a fornecer ao CONTRATADO
todas as informações necessárias ao mesmo para que este possa executar
suas funções, a cumprir todos os horários e normas internas do hotel,
salvo se houver estipulação expressa em contrário e a pagar pelos
serviços ora aqui contratados e os posteriormente solicitados.

*Parágrafo Segundo --* O CONTRATANTE obriga-se também a repassar a todos
os seus passageiros as informações abaixo:

* Horários: check-in à partir das 14:00 e check-out até 12:00.
* Café da manhã servido no restaurante das 06:00 às 10:00.
* O hotel disponibiliza de serviço de room servive 24 horas.
* O hotel possui estacionamento próprio mas com vagas limitadas com diária de
R$ 22,00 (vinte e dois reais).

**CLÁUSULA QUINTA --** Da negociação operacional e financeira.

*Parágrafo Primeiro --* A CONTRATANTE confirma o número de 72 hóspedes por dia
de 14 a 20 de julho de 2019, e acrescidos de mais 68 hóspedes por dia de 20 a
29 de julho de 2019 para todos os fins estipulados neste instrumento; devendo
quaisquer cancelamentos (que não o total) ou alterações serem efetuados com
antecedência mínima de 02 (dois) meses do início das hospedagens mediante
solicitação por escrito.

******

*Parágrafo Segundo --* Para a confirmação das reservas e evento será
necessário o pagamento antecipado de 70% (setenta por cento) do valor
total orçado para as hospedagens conforme cronograma abaixo, devendo os
restante do saldo em aberto ser pago até o dia 29 de julho de 2019.

* Valor de 14 a 29 de julho: 43 quartos x 15 dias x 2 hóspedes x R$ 85,00 =  R$ 109.650,00
* Valor de 20 a 29 de julho: 27 quartos x 09 dias x 2 hóspedes x R$ 85,00 =  R$  41.310,00

Total =  R$ 150.960,00

* 10% do valor total das hospedagens em 01 de outubro de 2018 -- R$ 15.096,00
* 30% do valor total das hospedagens em 01 de março de 2019 -- R$ 45.288,00
* 30% do valor total das hospedagens em 01 de julho de 2019 -- R$ 45.288,00

*Parágrafo Terceiro --* Para efeito dos cálculos estão sendo considerados 02
hóspedes por quarto. Se houver mais de dois hóspedes por quarto e/ou quartos com
apenas um hóspede, a diferença no valor será pago na última parcela.

*Parágrafo Quarto --* Se a CONTRATANTE não comparecer na data da
reserva anteriormente efetuada e não cancelar previamente dentro do
prazo estipulado no item Políticas de Cancelamento fica facultado ao
CONTRATADO o direito da cobrança do valor total orçado para o evento e
hospedagens a título de "no show", uma vez confirmadas às reservas.

*Parágrafo Quinto --* No caso de os passageiros da CONTRATANTE,
usufruírem de diárias em número superior ao período mencionado na
planilha, não havendo estipulação prévia, tendo estas diárias uma
diferenciação de preço, a mesma pagará a diferença apurada no período
utilizado fora do contratado.

*Parágrafo Sexto --* Ainda, fica resguardado ao CONTRATADO o direito de
receber metade do valor total das diárias aqui acordadas a título de "no
show" na hipótese de a programação da CONTRATANTE sofrer alterações por
caso fortuito ou motivo de força maior, tais como: condições climáticas
adversas, mudanças de horário dos atrativos previstos, atos de
terrorismo, roubos e furtos, aumentos de combustíveis, mudança na moeda
ou política econômica do Governo e etc.

*Parágrafo Sétimo --* O CONTRATADO não se responsabilizará por valores
e/ou objetos perdidos ou deixados nas áreas sociais do Hotel, em salas
de reuniões e no estacionamento, pois o mesmo dispõe de um cofre para a
guarda de valores e objetos.

*Parágrafo Oitavo --* Fica estabelecido ser a CONTRATANTE o responsável
pelo planejamento, organização e execução de sua programação, sendo o
mesmo o intermediário entre os seus passageiros e o CONTRATADO.

*Parágrafo Novo --* O CONTRATADO não responde, nem se solidariza, por
quaisquer atos, fatos ou eventos, onde a responsabilidade legal ou
contratual seja direta ou especifica da CONTRATANTE, como no caso dos
transportadores terrestres e empresas locais contratadas, somente
respondendo esta na forma da lei.

*Parágrafo Décido --* O presente instrumento particular não sujeita as
partes às leis trabalhistas e rege-se pelas disposições do Título VI --
Das Várias Espécies de Contrato, Capítulo VII -- Da Prestação de
Serviço, do Código Civil Brasileiro.

## IV -- RESCISÃO

**CLÁUSULA SEXTA --** Das políticas de cancelamento.

*Parágrafo Primeiro --* As partes comprometem-se a obedecer aos termos e
condições estabelecidas neste instrumento particular, sendo que na
hipótese de cancelamento, deverá prevalecer o abaixo.

*Parágrafo Segundo --* O presente contrato poderá ser denunciado
imotivadamente por qualquer uma das partes ou por infração de quaisquer
de suas cláusulas, mediante manifestação por escrito desde que observado
o prazo mínimo de 15 (quinze) dias a contar do recebimento da 1ª parcela
do pagamento, agendada para o dia 01 de outubro de 2018.

*Parágrafo Terceiro --* As partes acordam como deadline final o dia 31
de janeiro de 2019 para o cancelamento das hospedagens e salas de
eventos sem a aplicação de qualquer multa, sendo o adiantamento inicial
devolvido abatidas as taxas bancárias e da Administradora do Cartão via
depósito bancário em conta corrente da CONTRATANTE.

*Parágrafo Quarto --* Decorrido o prazo do deadline, o presente
instrumento poderá ser cancelado mediante pagamento de multa contratual
nas seguintes hipóteses:

1. Cancelamento efetuado até 28 de fevereiro de 2019 será cobrado o valor integral
da primeira parcela do pagamento.
1. Cancelamento efetuado até 30 de junho de 2019 será cobrado o valor integral
da primeira e segunda parcelas do pagamento.
1. Cancelamento efetuado até 10 de julho de 2019, será cobrado o valor integral
das três parcelas de pagamento efetuadas.

*Parágrafo Quinto --* O bloqueio e reserva uma vez confirmados, poderão
ser reduzidos na seguinte forma:

1. Redução de até no máximo 30% do total de apartamentos garantidos, sem ônus:
até 01 de março de 2019.
1. Após esta data serão cobradas as 05 (cinco) primeiras diárias dos apartamentos
reduzidos no limite máximo de 20% do total de apartamentos confirmados e até o
dia 01 de junho de 2019.
1. Após o dia 02 de junho não será possível a redução do bloqueio sem o
pagamento do valor total das diárias dos apartamentos reduzidos.

## V -- DISPOSIÇÕES FINAIS

**CLÁUSULA SÉTIMA --** Modificações

*Parágrafo Único --* As partes poderão modificar as cláusulas deste
instrumento no que se refere à quantidade de apartamentos postos à
disposição dos passageiros da CONTRATANTE, preços, períodos de
temporadas, condições de faturamento e prazos de pagamento, através de
anexo contratual numerado e com antecedência mínima de 02 (dois) meses
em se tratando se alterações substanciais.

**CLÁUSULA OITAVA --** Do pagamento.

*Parágrafo Primeiro --* Os pagamentos aqui acordados serão feitos todos
via débito em cartão de crédito fornecido pela CONTRATANTE.

*Parágrafo Segundo --* Para os devidos fins, este contrato está orçado
em R$ 150.960,00.

O presente contrato passa a vigorar a partir da data de sua assinatura
ou da confirmação por escrito do orçamento e/ou do recebimento deste,
haja vista este fazer parte integrante daquele.

Fica eleito o foro de Curitiba - PR, por mais privilegiado outros que possam
existir, para dirimir quaisquer dúvidas dele resultante, podendo as condições
ora pactuadas somente serem revistas após o seu implemento, quando ocorrerem
mudanças drásticas na Legislação Fiscal ou na política econômica do país.

Curitiba, 15 de setembro de 2018.



\   
\   
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
HOTEL NACIONAL INN CURITIBA  
Francisco Pedro Azambuja Vieira  

\   
\   
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
INSTITUTO PARA CONSERVAÇÃO DE TECNOLOGIA LIVRES  
Antonio Soares de Azevedo Terceiro  

\   
\   
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
Testemunha:  
CPF:  

\   
\
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
Testemunha:  
CPF:  

***Na versão final trocar as listas numeradas com números normais por números
romanos.***
